<?php

/**
 * Implements hook_install_tasks()
 *
 * Here we will define extra installation steps for our Site Editor profile.
 *
 * @param $install_state
 * @return array $tasks
 */
function site_editor_install_tasks(&$install_state)
{
    $tasks = array(
        'site_editor_role' => _site_editor_role_task(),
        'default_themes' => _default_themes_task(),
    );

    if ($install_state ['interactive']) {
        $tasks['site_locales'] = _site_locales_task();
        $tasks['import_translations'] = _site_import_translations_task();
    }

    return $tasks;
}

/**
 * Implements hook_install_tasks_alter()
 *
 * Removing the built-in language selection installation step, but choosing English as default for the installation process to work.
 */
function site_editor_install_tasks_alter(&$tasks, &$install_state)
{
    $tasks['install_select_locale']['function'] = 'select_eng_default';
    $tasks['install_select_locale']['display'] = FALSE;
}

////////////////////////
//  TASK DEFINITIONS  //
////////////////////////

/**
 * Extra installation step to import supported languages in batch
 *
 * @return array
 */
function _site_import_translations_task()
{
    return array(
        'display_name' => st('Import supported languages'),
        'display' => TRUE,
        'type' => 'batch',
        'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
        'function' => 'import_site_translations'
    );
}

/**
 * Extra installation step to ask the user for supported locales
 *
 * @return array
 */
function _site_locales_task()
{
    return array(
        'display_name' => st('Select supported locales'),
        'display' => TRUE,
        'type' => 'form',
        'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
        'function' => 'site_locales'
    );
}

/**
 * Task to disable the default themes and enable others
 *
 * @return array
 */
function _default_themes_task()
{
    return array(
        'display_name' => st('Enabling default themes'),
        'display' => TRUE,
        'type' => 'normal',
        'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
        'function' => 'enable_themes'
    );
}

/**
 * Task to create a new Site Editor role and assign it permissions.
 *
 * @return arra
 */
function _site_editor_role_task()
{
    return array(
        'display_name' => st('Creating Site Editor role'),
        'display' => TRUE,
        'type' => 'normal',
        'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
        'function' => 'create_site_editor_role'
    );
}


/////////////////
//  CALLBACKS  //
/////////////////

/**
 * Call back function to skip the standard language selection step.
 * Defaults to English. Additional locale choices can be made later in the process.
 */
function select_eng_default(&$install_state)
{
    $install_state['parameters']['locale'] = 'en';
}

/**
 * Import the user selected locales
 */
function import_site_translations()
{
    // this function can be used to do some useful stuff with the user selected locales
}

/**
 * Offer the user a selection of supported locales
 */
function site_locales()
{
    // Provides predefined country list.
    include_once DRUPAL_ROOT . '/includes/iso.inc';

    $form['translations'] = array(
        '#type' => 'select',
        '#title' => st('Additional languages'),
        '#description' => st('Select additional languages to enable and download contributed interface translations.'),
        '#options' => _country_get_predefined_list(),
        '#multiple' => TRUE,
        '#size' => 10,
    );

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => st('Install selected languages'),
    );

    return $form;
}

/**
 * Keep track of the selected languages to do the import in batch
 */
function site_locales_submit(&$form, &$form_state)
{
    variable_set('supported_locales',
        $form_state['values']['translations']);
}

/**
 * Create a site editor role for site administrators, with all available permissions assigned.
 */
function create_site_editor_role()
{
    // Make the first role
    $site_editor_role = new stdClass;
    $site_editor_role->name = 'Site Editor';
    $site_editor_role->weight = 3;
    user_role_save($site_editor_role);
    $roleid = $site_editor_role->rid;

    // Permissions to assign to the role.
    $perms = array(
        'access content',
    );

    // Grant the permissions. This function takes care of all necessary cache resets
    user_role_grant_permissions($roleid, $perms);

    // As an example we assign user 1 the "site editor" role.
    db_insert('users_roles')
        ->fields(array('uid' => 1, 'rid' => $site_editor_role->rid))
        ->execute();
}

/**
 * Change the default public theme to bootstrap, admin theme to seven
 */
function enable_themes()
{
    $enable = array(
        'theme_default' => 'bootstrap',
        'admin_theme' => 'seven',
    );
    theme_enable($enable);

    foreach ($enable as $var => $theme) {
        if (!is_numeric($var)) {
            variable_set($var, $theme);
        }
    }

    theme_disable(array('bartik'));
}