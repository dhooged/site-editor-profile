Example site installation:

    > drush make site-with-custom-profile.make www
    > cd www
    > drush si site_editor --db-url=mysql://root@localhost/profile --account-name=admin --account-pass=admin --site-name="Drupal With Custom Profile" --db-su=root -y